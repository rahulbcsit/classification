﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.Controls;
using Accord.IO;
using Accord.Math;
using Accord.Statistics.Distributions.Univariate;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.MachineLearning.Bayes;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.Neuro.Learning;
using Accord.Neuro;
using Accord.Neuro.Networks;
using Accord.Statistics.Kernels;




namespace FireClassification
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SimpleClassification_Click(object sender, EventArgs e)
        {
            DataTable table = new ExcelReader("examples.xls").GetWorksheet("Classification - Yin Yang");

            // Convert the DataTable to input and output vectors
            double[][] inputs = table.ToArray<double>("X", "Y");
            int[] outputs = table.Columns["G"].ToArray<int>();

            // Plot the data
            ScatterplotBox.Show("Yin-Yang", inputs, outputs).Hold();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable table = new ExcelReader("examples.xls").GetWorksheet("Classification - Yin Yang");

            // Convert the DataTable to input and output vectors
            double[][] inputs = table.ToArray<double>("X", "Y");
            int[] outputs = table.Columns["G"].ToArray<int>();
            var teacher = new LinearCoordinateDescent();

            // Teach the vector machine
            SupportVectorMachine svm = teacher.Learn(inputs, outputs);

            // Classify the samples using the model
            bool[] answers = svm.Decide(inputs); 

            // Optional: Compute the distance from the decision hyperplane for each sample:
            double[] distances = svm.Score(inputs);

            // Plot the results
            ScatterplotBox.Show("Expected results", inputs, outputs);
           // ScatterplotBox.Show("LinearSVM results", inputs, answers);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable table = new ExcelReader("examples.xls").GetWorksheet("Classification - Yin Yang");

            // Convert the DataTable to input and output vectors
            double[][] inputs = table.ToArray<double>("X", "Y");
            int[] outputs = table.Columns["G"].ToArray<int>();

            var learner = new NaiveBayesLearning<NormalDistribution>();

            // Estimate the Naive Bayes
            var nb = learner.Learn(inputs, outputs);

            // Classify the samples using the model
            int[] answers = nb.Decide(inputs);

            // Plot the results
            ScatterplotBox.Show("Expected results", inputs, outputs);
            ScatterplotBox.Show("Naive Bayes results", inputs, answers)
                .Hold();
        }

        private void button5_Click(object sender, EventArgs e)
        {
           /* DataTable table = new ExcelReader("examples.xls").GetWorksheet("Classification - Yin Yang");

            // Convert the DataTable to input and output vectors
            double[][] inputs = table.ToArray<double>("X", "Y");
            int[] outputs = table.Columns["G"].ToArray<int>();
            // Create a Gaussian binary support machine with 2 inputs
            var svm = new KernelSupportVectorMachine<Gaussian>();

            // Create a new Sequential Minimal Optimization (SMO) learning 
            // algorithm and estimate the complexity parameter C from data
            var teacher = new SequentialMinimalOptimization<Gaussian>()
            {
                UseComplexityHeuristic = true,
                UseKernelEstimation = true // Estimate the kernel from the data
            };

            // Teach the vector machine
            SupportVectorMachine<Gaussian> svm = teacher.Learn(inputs, outputs);

            // Optional: Compute the distance from the decision hyperplane for each sample:
            double[] distances = svm.Score(inputs);

            // Plot the results
            ScatterplotBox.Show("Expected results", inputs, outputs);
            ScatterplotBox.Show("GaussianSVM results", inputs, answers); */
        }

        private void button3_Click(object sender, EventArgs e) { 


             DataTable table = new ExcelReader("examples.xls").GetWorksheet("Classification - Yin Yang");

        // Convert the DataTable to input and output vectors
        double[][] inputs = table.ToArray<double>("X", "Y");
        int[] outputs = table.Columns["G"].ToArray<int>();
        DecisionTree tree = new DecisionTree(
                inputs: new List<DecisionVariable>
                    {
                        DecisionVariable.Continuous("X"),
                        DecisionVariable.Continuous("Y")
                    },
                classes: 2);

            C45Learning teacher = new C45Learning(tree);

            teacher.Learn(inputs, outputs);

            // Classify the samples using the model
            int[] answers = tree.Decide(inputs);

            // Plot the results
            ScatterplotBox.Show("Expected results", inputs, outputs);
            ScatterplotBox.Show("Decision Tree results", inputs, answers)
                .Hold();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DataTable table = new ExcelReader("examples.xls").GetWorksheet("Classification - Yin Yang");

            // Convert the DataTable to input and output vectors
            double[][] inputs = table.ToArray<double>("X", "Y");
            int[] outputs = table.Columns["G"].ToArray<int>();
            IActivationFunction function = new BipolarSigmoidFunction();

            // In our problem, we have 2 inputs (x, y pairs), and we will 
            // be creating a network with 5 hidden neurons and 1 output:
            //
            var network = new ActivationNetwork(function,
                inputsCount: 2, neuronsCount: new[] { 5, 1 });

            // Create a Levenberg-Marquardt algorithm
            var teacher = new LevenbergMarquardtLearning(network)
            {
                UseRegularization = true
            };

            // Because the network is expecting multiple outputs,
            // we have to convert our single variable into arrays
            //
            var y = outputs.ToDouble().ToArray();

            // Iterate until stop criteria is met
            double error = double.PositiveInfinity;
            double previous;

            do
            {
                previous = error;

                // Compute one learning iteration
                error = teacher.RunEpoch(inputs, y);

            } while (Math.Abs(previous - error) < 1e-10 * previous);


            // Classify the samples using the model
            int[] answers = inputs.Apply(network.Compute).GetColumn(0).Apply(System.Math.Sign);

            // Plot the results
            ScatterplotBox.Show("Expected results", inputs, outputs);
            ScatterplotBox.Show("Network results", inputs, answers)
            .Hold();


        }
    }
}
